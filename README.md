# CTM Radio Public API #

Welcome to the CTM Radio Public API documentation, here you will find everything
you need to know about it.

## What is an API for? ##

An **A**pplication **P**rogramming **I**nterface, is a computing interface which
defines interactions between multiple software intermediaries. In this case, it
can easily provide you with the information of what is playing at the radio for
the interface, application or service you're working with.

## What is the data format? ##

Like most known APIs, we use JSON as the standard data transport format. More
info at [Wikipedia](https://wikipedia.org/wiki/JSON).

## What data do I receive? ##

Here's a simplified example of the data you recieve from the API when you do a
regular call to it.

```json
{
    "album_id": 123,
    "duration": 234.56789012345678,
    "year": 2020,
    "genre": "Genre",
    "id": 1234,
    "album": "Album",
    "license": {
        "url": "http://creativecommons.org/licenses/by-sa/3.0/",
        "shortname": "CC BY-SA 3.0",
        "name": "Creative Commons Attribution-ShareAlike 3.0 Unported"
    },
    "artist": "Artist",
    "url": "http://artistwebsite.com",
    "country": "Country",
    "title": "Title",
    "cover": "data:image/png;base64,YUxabj22u9SRKiM44Ek5ESWx7TaQeZjqfTxkw3rHfkcatgtWtX3w9ifUuWpcDRTf...",
    "isLive": false
}
```

Let's explain these one by one:

> Entries indicated in italics refer to **incomplete information**, that is,
> only some albums include the indicated data, if there's none, the API returns
> an empty string for that field. This is early access info, and is being
> cataloged and added little by little. It will be announced when this
> information has been completed.

* **`artist`** is the name of the song's artist(s).
* **`title`** is the name of the song.
* **`album`** is the name of the album.
* **`id`** is the identification number of the song. Used as an argument to a
`/preview/` call.
* **`cover`** is the cover art image, encoded as base64 string. This data can vary
in length, depending on the value passed to the `img_size` argument.
* ***`genre`** is the album musical genre*.
* **`country`** is the name of the artist(s) country.
* **`year`** is the album release year.
* ***`url`** is the web address where you can download/buy the album.*
* **`license`** contains the song's licensing information:
    * **`name`** is the license full name, as defined by the license.
    * **`shortname`** is the license code name, as defined by the license.
    * **`url`** is the URL to the definition of the license of the song.
* **`duration`** is the estimated duration in seconds of the song.
* **`isLive`** will be `true` when there's a live show running on the radio, and
`false` when there's the regular unnatended content.

Plus, when there's a live show, the following fields will be included:

```json
{
  "broadcaster": "Broadcaster's Name",
  "show": "Show's Name"
}
```

* **`broadcaster`** is the name of the caster running the show.
* **`show`** is the name of the live show.

These data are the sole responsibility of the caster, who commits to set them up
them correctly in the application used to broadcast.

## What's that long weird string in the data? ##

You're probably referring to the `cover` field. It contains the album's cover
art image, encoded using [base64](http://en.wikipedia.org/wiki/Base64). The
image itself is encoded in PNG format, using an
[indexed palette](https://en.wikipedia.org/wiki/Indexed_color)

## Ok, how can I access the API? ##

**The API is available at https://api.ctmradio.org/**.

## Which are the available functions? ##

| Function | Filetype returned | Returned contents | Returned size |
|:-:|:-:|:-:|:-:|
| **`/cover/<id>/`** | JSON | `{"cover": "data:image/png;base64,..."}` | 400px, unless `?img_size` is used |
| **`/cover/<id>.png`** | PNG | A ready-to-serve file with `Content-Type: image/png` header | 400px, unless `?img_size` is used |
| **`/preview/<id>/`** | JSON | `{"preview": "data:audio/ogg;base64,..."}` | 10 seconds |
| **`/preview/<id>.ogg`** | OGG | A ready-to-serve file with `Content-Type: audio/ogg` header | 10 seconds |
| **`/album/<id>/`** | JSON | An array with all songs of the album identified with `id` | Variable |
| **`/catalog/`** | JSON | An array with the entire musical repository | Variable |

## Which are the allowed arguments? ##

* **`/?img_size=tamaño`**: Used to define the album's cover art image size in
pixels (default: 400). Value must be between 50 and 400. Example:
https://api.ctmradio.org/?img_size=200
* **`/?no_cover`**: Pass this flag to receive the API's repsonse without the
album's cover art. Useful for text-only applications. Example:
https://api.ctmradio.org/?no_cover

## Do you have an example of how it is used? ##

Using [jQuery](http://jquery.com/):

```javascript
// Calling the API
$.getJSON("https://api.ctmradio.org/", function (data) {
    // Once it finished loading, you can start processing the data received
    console.log(data.artist);
    // To show the cover art, you may do the following
    $("<img/>").attr('src', data.cover);
});
```

Using PHP:

```php
<?php
    // Calling the API
    $data = json_decode(file_get_contents("https://api.ctmradio.org/"));
    // Once it finished loading, you can start processing the data received
    echo $data->artist;
    // To show the cover art, you may do the following
    echo "<img src='".$data->cover."' alt='' />";
?>
```

Using Python:

```python
import requests

# Calling the API
r = requests.get('https://api.ctmradio.org/')
# Once it finished loading, you can start processing the data received
print r.json()["artist"].encode("utf-8")
# To show the cover art, you may do the following
from PIL import Image
import base64
try:
    from cStringIO import StringIO
except:
    from StringIO import StringIO
image = Image.open(StringIO(base64.b64decode(r.json()["cover"].split(",")[1])))
image.show()

```

## Anything else? ##

Yes. This documentation is under development, as well as the API, so you can
help or get help by
[adding an incidence](https://gitlab.com/breadmaker/rctm-api/-/issues/new) in
this repository. Thanks in advance for your feedback.
