#!/usr/bin/python3
import sys
import logging

# UNCOMMENT THE NEXT LINES TO ACTIVATE VIRTUALENV
#activate_this = '/path/to/virtualenv/bin/activate_this.py'
#execfile(activate_this, dict(__file__=activate_this))

logging.basicConfig(stream=sys.stderr)
sys.path.insert(0,"/path/to/the/api/")

# IMPORTANT: ADD YOUR OWN RANDOM KEYWORD AS A SALT KEY
from api import app as application
application.secret_key = 'ADD_YOUR_SALT_HERE'
