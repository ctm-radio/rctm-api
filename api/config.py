ADMINS = ['admin1@example.com', 'admin2@example.com']
DB_USER='database_username'
DB_PASSWORD='database_password'
DB_HOST='database_hostname'
SENTRY_DSN = 'https://url.to.dsn.of.sentry.io'
