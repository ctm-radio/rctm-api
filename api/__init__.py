#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#  rctm-api - CTM Radio Python 3 API
#
#  Version 4.0.0
#
#  Copyleft 2014 - 2020 Felipe Peñailillo (@breadmaker) <breadmaker@ctmradio.org>
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.

import os, string, random, json, gzip, requests, base64
from mysql import connector
from flask import Flask, request, send_from_directory, make_response, \
    render_template, after_this_request, abort, send_file
from collections import namedtuple
from datetime import datetime
from io import BytesIO
from PIL import Image
from functools import wraps, update_wrapper
from operator import itemgetter
from itertools import groupby
from raven.contrib.flask import Sentry
from config import ADMINS, DB_USER, DB_PASSWORD, DB_HOST, SENTRY_DSN

app = Flask(__name__)
# This app is monitored by Sentry
sentry = Sentry(app, dsn=SENTRY_DSN)

# Adding support to send error alerts via email
if not app.debug:
    import logging
    from logging.handlers import SMTPHandler
    mail_handler = SMTPHandler('127.0.0.1',
                               'api-errors@ctmradio.org',
                               ADMINS, 'API error')
    mail_handler.setLevel(logging.ERROR)
    app.logger.addHandler(mail_handler)

# Redirects requests for favicon.ico to favicon.png
@app.route('/favicon.ico')
def favicon():
    return send_from_directory(os.path.join(app.root_path, 'static'),
                               'favicon.png', mimetype='image/png')

# Defines a simple ID generator
def id_generator(size=8, chars=string.ascii_lowercase + string.ascii_uppercase + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))

# Defines uncached JSON response headers
def uncachedjsonheaders(view):
    @wraps(view)
    def global_headers(*args, **kwargs):
        response = make_response(view(*args, **kwargs))
        # Allows API access from any domain
        response.headers['Access-Control-Allow-Origin'] = '*'
        response.headers['Access-Control-Allow-Headers'] = 'Content-Type'
        response.headers['Access-Control-Allow-Methods'] = 'GET, POST, OPTIONS'
        # Temporary code to prevent caching of this API response
        # TODO: THIS MUST BE HANDLED BETTER
        response.headers['Last-Modified'] = datetime.utcnow().strftime("%a, %d %b %Y %X GMT")
        response.headers['Cache-Control'] = 'no-store, no-cache, must-revalidate, post-check=0, pre-check=0, max-age=0'
        response.headers['Pragma'] = 'no-cache'
        response.headers['Expires'] = '-1'
        # Returns correct Content Type to proper handling
        response.headers['Content-Type'] = 'application/json; charset=UTF-8'
        # Adding a temporary Transaction ID, this will replaced by an API key in the future
        # TODO: ADDING API KEY SUPPORT WILL RESULT IN BETTER REQUEST THROTTLING
        response.headers['Transaction-ID'] = id_generator()
        return response
    return update_wrapper(global_headers, view)

# Defines multimedia response headers
def mediaheaders(view):
    @wraps(view)
    def global_headers(*args, **kwargs):
        response = make_response(view(*args, **kwargs))
        response.headers['Access-Control-Allow-Origin'] = '*'
        response.headers['Access-Control-Allow-Headers'] = 'Content-Type'
        response.headers['Access-Control-Allow-Methods'] = 'GET, POST, OPTIONS'
        # Cache max age is 24 hours
        response.headers['Cache-Control'] = 'max-age=86400'
        response.headers['Transaction-ID'] = id_generator()
        return response
    return update_wrapper(global_headers, view)

# Defines cached JSON response headers
def jsonheaders(view):
    @wraps(view)
    def global_headers(*args, **kwargs):
        response = make_response(view(*args, **kwargs))
        response.headers['Access-Control-Allow-Origin'] = '*'
        response.headers['Access-Control-Allow-Headers'] = 'Content-Type'
        response.headers['Access-Control-Allow-Methods'] = 'GET, POST, OPTIONS'
        # Cache max age is 24 hours
        response.headers['Cache-Control'] = 'max-age=86400'
        response.headers['Content-Type'] = 'application/json; charset=UTF-8'
        response.headers['Transaction-ID'] = id_generator()
        return response
    return update_wrapper(global_headers, view)

# Defines error pages response headers
def errorheaders(view):
    @wraps(view)
    def global_headers(*args, **kwargs):
        response = make_response(view(*args, **kwargs))
        response.headers['Access-Control-Allow-Origin'] = '*'
        response.headers['Access-Control-Allow-Headers'] = 'Content-Type'
        response.headers['Access-Control-Allow-Methods'] = 'GET, POST, OPTIONS'
        # Cache max age is 365 days
        response.headers['Cache-Control'] = 'Cache-Control: max-age=31556926'
        response.headers['Transaction-ID'] = id_generator()
        return response
    return update_wrapper(global_headers, view)

# Defines compressed response
def gzipped(f):
    @wraps(f)
    def view_func(*args, **kwargs):
        @after_this_request
        def zipper(response):
            accept_encoding = request.headers.get('Accept-Encoding', '')
            if 'gzip' not in accept_encoding.lower():
                return response
            response.direct_passthrough = False
            if (response.status_code < 200 or
                response.status_code >= 300 or
                'Content-Encoding' in response.headers):
                return response
            gzip_buffer = BytesIO()
            gzip_file = gzip.GzipFile(mode='wb', fileobj=gzip_buffer)
            gzip_file.write(response.data)
            gzip_file.close()
            response.data = gzip_buffer.getvalue()
            response.headers['Content-Encoding'] = 'gzip'
            response.headers['Vary'] = 'Accept-Encoding'
            response.headers['Content-Length'] = len(response.data)
            return response
        return f(*args, **kwargs)
    return view_func

# Defines a custom dictionary factory for database query responses
def dict_factory(cursor, row):
    d = {}
    for idx, col in enumerate(cursor.description):
        d[col[0]] = row[idx]
    return d

# Defines a method to resize the cover art
def resize_cover(path, size, as_base64=True):
    image = Image.open(path)
    image = image.convert('RGB')
    if size:
        # Limiting input size
        if size < 50:
            size = 50
        elif size > 400:
            size = 400
        # If size is exactly 400, there's no need to resize
        if size < 400:
            image.thumbnail((size, size), Image.ANTIALIAS)
    # Saving changes
    thumb = BytesIO()
    image.save(thumb, format="PNG", optimize=True)
    thumb.seek(0)
    if as_base64:
        return "data:image/png;base64," + str(base64.b64encode(thumb.getvalue()).decode('utf-8'))
    else:
        return thumb

db = namedtuple('db', 'conn cur')

# Connects to the database and sets up a cursor
def db_connect():
    conn = connector.connect(user=DB_USER,
                             password=DB_PASSWORD,
                             host=DB_HOST,
                             database="ctmradio")
    cur = conn.cursor(dictionary=True)
    return db(conn = conn, cur = cur)

@app.route('/', methods=['GET'])
@uncachedjsonheaders
@gzipped
def index():
    try:
        now_playing = requests.get("http://localhost:8000/json.xsl")
    except Exception as e:
        return json.dumps({"error": "Can't connect to Icecast server."}, separators=(',', ':')), 500
    if now_playing:
        icecast_data = json.loads(now_playing.text)
        if len(icecast_data) > 0:
            # Initializing variables
            isLive = False
            broadcaster = ""
            show = ""
            artistName = request.args.get("artist")
            songTitle = request.args.get("title")
            for item in icecast_data:
                if item == "/ctmradio.ogg" and \
                (artistName == None and songTitle == None):
                    artistName = icecast_data["/ctmradio.ogg"]["artist"]
                    songTitle = icecast_data["/ctmradio.ogg"]["title"]
                elif item == "/live.ogg":
                    isLive = True
                    broadcaster = icecast_data["/live.ogg"]["broadcaster"]
                    show = icecast_data["/live.ogg"]["show"]
            if not artistName and songTitle:
                tmp = songTitle.split(" - ")
                if len(tmp) == 1:
                    songTitle = tmp[0]
                    artistName = "Unknown artist"
                else:
                    artistName = tmp[0]
                    songTitle = tmp[1]
        else:
            return json.dumps({"error": "Radio is not online right now, please try again later."}, separators=(',', ':')), 500
    query = ("SELECT ar.artist_name AS artist, s.song_name AS title,"
            " al.album_name AS album, s.song_id AS id, al.album_id,"
            " al.album_path AS path, g.genre_name AS genre, c.country_name"
            " AS country, al.album_year AS year, al.album_url AS url,"
            " l.license_url, l.license_name, l.license_shortname,"
            " s.song_duration AS duration FROM artist AS ar, song AS s,"
            " album AS al, genre AS g, country AS c, license AS l WHERE"
            " ar.artist_name = %(name)s AND s.song_name = %(title)s AND"
            " s.artist_id = ar.artist_id AND al.album_id = s.album_id AND"
            " al.genre_id = g.genre_id AND ar.country_id = c.country_id AND"
            " s.license_id = l.license_id")
    db = db_connect()
    db.cur.execute(query, {'name': artistName, 'title': songTitle})
    db_data = db.cur.fetchone()
    db.conn.close()
    if db_data:
        if request.args.get("no_cover") != None:
            cover = ""
        else:
            try:
                cover = resize_cover(os.path.join(db_data["path"], "Cover.png"),
                    int(request.args.get("img_size")))
            except Exception:
                cover = resize_cover(os.path.join(db_data["path"], "Cover.png"),
                    400)
        result = {"artist": db_data["artist"],
                  "title": db_data["title"],
                  "album": db_data["album"],
                  "id": db_data["id"],
                  "album_id": db_data["album_id"],
                  "cover": cover,
                  "genre": db_data["genre"],
                  "country": db_data["country"],
                  "year": db_data["year"],
                  "url": db_data["url"],
                  "duration": db_data["duration"],
                  "license": {
                      "url": db_data["license_url"],
                      "name": db_data["license_name"],
                      "shortname": db_data["license_shortname"]
                  },
                  "isLive": isLive}
        if isLive:
            result["broadcaster"] = broadcaster
            result["show"] = show
        return json.dumps(result, separators=(',', ':'))
    else:
        if request.args.get("no_cover") != None:
            cover = ""
        else:
            try:
                cover = resize_cover(os.path.join(app.root_path,
                    'static/logo.png'), int(request.args.get("img_size")))
            except Exception:
                cover = resize_cover(os.path.join(app.root_path,
                    'static/logo.png'), 400)
        result = {"artist": "" if not artistName else artistName,
                  "title": "" if not songTitle else songTitle,
                  "album": "CTM Radio",
                  "id": 0,
                  "album_id": 0,
                  "cover": cover,
                  "genre": "",
                  "country": "Earth",
                  "year": datetime.now().strftime("%Y"),
                  "url": "https://ctmradio.org",
                  "duration": 0,
                  "license": "",
                  "isLive": isLive}
        if isLive:
            result["broadcaster"] = broadcaster
            result["show"] = show
        return json.dumps(result, separators=(',', ':'))

# Query and returns the complete radio catalog
@app.route('/catalog/', methods=['GET'])
@jsonheaders
@gzipped
def get_catalog():
    # Please note that one album may have more than one artist, in that case the
    # query will return duplicate results for that particular album, meaning if
    # an album has four different artists, the query will return 4 results for
    # that album, every result containing a different artist name
    query = ("SELECT al.album_id AS id, al.album_name AS name,"
            " ar.artist_name AS artists, al.album_year AS year,"
            " al.album_url AS url, g.genre_name AS genre, c.country_name AS"
            " country FROM album AS al, artist AS ar, song AS s, genre AS"
            " g, country AS c WHERE s.artist_id = ar.artist_id AND"
            " s.album_id = al.album_id AND al.genre_id = g.genre_id AND"
            " c.country_id = ar.country_id GROUP BY al.album_id,"
            " s.artist_id ORDER BY LOWER(al.album_name)")
    db = db_connect()
    db.cur.execute(query)
    db_res = db.cur.fetchall()
    db.conn.close()
    getAlbum = itemgetter('name')
    catalog = []
    is_first = False
    for key, album in groupby(db_res, getAlbum):
        is_first = True
        entry = {}
        for record in album:
            if is_first:
                for item in record:
                    if item == "artists":
                        entry[item] = [record[item]]
                    else:
                        entry[item] = record[item]
                is_first = False
            else:
                entry["artists"].append(record["artists"])
        catalog.append(entry)
    return json.dumps(catalog, separators=(',', ':'))

# Fetches and returns an album identified by its number ID
@app.route('/album/<int:album_id>/', methods=['GET'])
@jsonheaders
@gzipped
def get_album(album_id):
    query = ("SELECT s.song_id AS id, s.song_name AS name, a.artist_name AS"
            " artist, l.license_shortname AS license FROM song AS s, artist"
            " AS a, license AS l WHERE s.artist_id = a.artist_id AND"
            " s.license_id = l.license_id AND s.album_id = %(id)s")
    db = db_connect()
    db.cur.execute(query, {'id': album_id})
    response = db.cur.fetchall()
    db.conn.close()
    if response != None:
        return json.dumps(response, separators=(',', ':'))
    abort(400)

# Returns an iterable JSON with results from the <term> query
@app.route('/search/<term>/', methods=['GET'])
@jsonheaders
@gzipped
def search(term):
    # Search query MUST be 3 characters or more
    if len(term) >= 3:
        # Songs
        query = ("SELECT s.song_name AS name, al.album_id AS id,"
                " ar.artist_name AS artist FROM song AS s, album AS al,"
                " artist AS ar WHERE LOWER(s.song_name) LIKE LOWER(%(term)s)"
                " AND s.album_id = al.album_id AND s.artist_id ="
                " ar.artist_id")
        db = db_connect()
        db.cur.execute(query, {'term': "%" + term + "%"})
        results = { "songs" : db.cur.fetchall() }
        # Artists
        query = ("SELECT DISTINCT(ar.artist_name) AS name, c.country_name"
                " AS country FROM artist AS ar, country as c WHERE"
                " LOWER(ar.artist_name) LIKE LOWER(%(term)s) AND ar.country_id"
                " = c.country_id")
        db.cur.execute(query, {'term': "%" + term + "%"})
        results["artists"] = db.cur.fetchall()
        # Countries
        query = ("SELECT c.country_name AS name FROM country as c WHERE"
                " LOWER(c.country_name) LIKE LOWER(%(term)s)")
        db.cur.execute(query, {'term': "%" + term + "%"})
        results["country"] = db.cur.fetchall()
        # Genres
        query = ("SELECT genre_name AS name FROM genre WHERE"
                " LOWER(genre_name) LIKE LOWER(%(term)s)")
        db.cur.execute(query, {'term': "%" + term + "%"})
        results["genres"] = db.cur.fetchall()
        query = ("SELECT album_id AS id FROM album WHERE genre_id ="
                " (SELECT genre_id FROM genre WHERE genre_name ="
                " %(genre)s)")
        for i, genre in enumerate(results["genres"]):
            db.cur.execute(query, {'genre': genre["name"]})
            results["genres"][i]["albums"] = db.cur.fetchall()
        # Years
        query = ("SELECT DISTINCT album_year AS year FROM album WHERE"
                " album_year LIKE %(term)s")
        db.cur.execute(query, {'term': "%" + term + "%"})
        results["years"] = db.cur.fetchall()
        query = "SELECT album_id AS id FROM album WHERE album_year = %(year)s"
        for i, year in enumerate(results["years"]):
            db.cur.execute(query, {'year': year["year"]})
            results["years"][i]["albums"] = db.cur.fetchall()
            db.conn.close()
        return json.dumps(results, separators=(',', ':'))
    else:
        return json.dumps([])

# Fetches and return a base64 encoded song preview identified by <song_id>
@app.route('/preview/<int:song_id>/', methods=['GET'])
@jsonheaders
@gzipped
def get_preview(song_id):
    query = "SELECT song_path AS path FROM song WHERE song_id = %(song)s"
    db = db_connect()
    db.cur.execute(query, {'song': song_id})
    response = db.cur.fetchone()
    db.conn.close()
    if response != None:
        with open(response["path"].replace(".ogg", "_preview.ogg"), 'rb') as preview_file:
            encoded_preview = base64.b64encode(preview_file.read())
            preview = "data:audio/ogg;base64,{0}".format(encoded_preview.decode('ascii'))
        return json.dumps({"preview": preview}, separators=(',', ':'))
    abort(400)

# Fetches and return an .ogg encoded song preview file identified by <song_id>
@app.route('/preview/<int:song_id>.ogg', methods=['GET'])
@mediaheaders
@gzipped
def get_preview_file(song_id):
    query = "SELECT song_path AS path FROM song WHERE song_id = %(song)s"
    db = db_connect()
    db.cur.execute(query, {'song': song_id})
    response = db.cur.fetchone()
    db.conn.close()
    if response != None:
        return send_file(response["path"].replace(".ogg", "_preview.ogg"))
    abort(400)

# Fetches and return a base64 encoded album cover art identified by <album_id>
@app.route('/cover/<int:album_id>/', methods=['GET'])
@jsonheaders
@gzipped
def get_cover(album_id):
    query = "SELECT album_path AS path FROM album WHERE album_id = %(album)s"
    db = db_connect()
    db.cur.execute(query, {'album': album_id})
    response = db.cur.fetchone()
    db.conn.close()
    if response != None:
        path = response["path"]
        path = os.path.join(path, "Cover.png")
        if request.args.get("img_size"):
            cover = resize_cover(path, int(request.args.get("img_size")))
        else:
            cover = resize_cover(path, 400)
        return json.dumps({"cover": cover.replace("\n", "")}, separators=(',', ':'))
    else:
        if request.args.get("img_size"):
            cover = resize_cover(os.path.join(app.root_path,
                    'static/logo.png'), int(request.args.get("img_size")))
        else:
            cover = resize_cover(os.path.join(app.root_path,
                    'static/logo.png'), 400)
        return json.dumps({"cover": cover.replace("\n", "")}, separators=(',', ':'))
    abort(400)

# Fetches and return a .png encoded album cover art identified by <album_id>
@app.route('/cover/<int:album_id>.png', methods=['GET'])
@mediaheaders
@gzipped
def get_cover_file(album_id):
    query = "SELECT album_path AS path FROM album WHERE album_id = %(album)s"
    db = db_connect()
    db.cur.execute(query, {'album': album_id})
    response = db.cur.fetchone()
    db.conn.close()
    if response != None:
        path = response["path"]
        path = os.path.join(path, "Cover.png")
        if request.args.get("img_size"):
            cover = resize_cover(path,
                int(request.args.get("img_size")), False)
            return send_file(cover, mimetype='image/png')
        else:
            return send_file(path)
    else:
        if request.args.get("img_size"):
            cover = resize_cover(os.path.join(app.root_path,
                    'static/logo.png'), int(request.args.get("img_size")),
                     False)
        else:
            cover = resize_cover(os.path.join(app.root_path,
                    'static/logo.png'), 400, False)
        return send_file(cover, mimetype='image/png')
    abort(400)

# Define error handlers
@app.errorhandler(404)
@errorheaders
@gzipped
def show_404(e):
    return render_template('404.html'), 404

@app.errorhandler(400)
@errorheaders
@gzipped
def show_400(e):
    return render_template('400.html'), 400

@app.errorhandler(500)
@errorheaders
@gzipped
def show_500(e):
    return render_template('500.html'), 500

if __name__ == '__main__':
    app.run()
